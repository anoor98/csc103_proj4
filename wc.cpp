/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

#include <string>
#include <iostream>
using std::cout;
using std::cin;
#include <fstream>
using std::ifstream;
using std::ofstream;
using std::ios;
#include <cstdlib>
using std::string;
#include <set>
using std::set;
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf
#include <stdio.h>
#include <vector>
using namespace std;

size_t wordCounter(string fileName);
size_t linesCounter(string fileName);
size_t byteCounter(string fileName);
size_t maxLineLengthCounter(string fileName);
size_t uWordsCounter(string fileName);

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

int main(int argc, char *argv[])
{
	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	int flag = 0;

	 string newFile = "myFile.txt";

	ofstream outputFile(newFile);
	 string strFile;

	 while(getline(cin, strFile)){
			outputFile << strFile << "\n";
	 }
	 outputFile.close();

	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				charonly = byteCounter(newFile);
				flag++;
				cout << charonly << "\t";
				break;
			case 'l':
				linesonly = linesCounter(newFile);
				flag++;
				cout << linesonly << "\t";
				break;
			case 'w':
				wordsonly = wordCounter(newFile);
				flag++;
				cout << wordsonly << "\t";
				break;
			case 'u':
				uwordsonly = uWordsCounter(newFile);
				flag++;
				cout << uwordsonly << "\t";
				break;
			case 'L':
				longonly = maxLineLengthCounter(newFile);
				flag++;
				cout << longonly << "\t";
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	if(!flag){

		cout << "\t" << wordCounter(newFile) << "\t" << linesCounter(newFile) << "\t"
				 << byteCounter(newFile) << "\n" ;

	}
	// TODO: write me...
	
	return 0;
}
	//////////////////////////////////////////////////////////////
  size_t wordCounter(string fileName){
		int counter = 0;
		//vector<string> vecWords;

    ifstream inputFile(fileName.c_str(), ios::in);
	  string str;
    while(!inputFile.eof()){
        inputFile >> str;
				if(inputFile.eof())
					break;
        //vecWords.push_back(str);
				counter++;
    }

    inputFile.close();

    //return vecWords.size();
		return counter;
}
	/////////////////////////////////////////////////////////////
	size_t linesCounter(string fileName){
		int counter = 0;
		//vector<string> vecLines;

		ifstream inputFile(fileName.c_str(), ios::in);
		string str;
		while(!inputFile.eof()){
			getline(inputFile, str);
			if(inputFile.eof())
				break;
			//vecLines.push_back(str);
			counter++;
		}
		inputFile.close();

		//return vecLines.size();
		return counter;

	}

	////////////////////////////////////////////////////////////
	size_t byteCounter(string fileName){
		int counter = 0;
		//vector<char> vecBytes;

		ifstream inputFile(fileName.c_str(), ios::in);
		char ch;
		while(!inputFile.eof()){
			ch = inputFile.get();
			if(inputFile.eof())
				break;
			//vecBytes.push_back(ch);
			counter++;
		}
		inputFile.close();

		//return vecBytes.size();
		return counter;
	}

	//////////////////////////////////////////////////////////////
	size_t maxLineLengthCounter(string fileName){
		vector<string> vecLineLength;

		ifstream inputFile(fileName.c_str(), ios::in);
		string line;
		while(!inputFile.eof()){
			getline(inputFile, line);
			if(inputFile.eof())
				break;
			vecLineLength.push_back(line);
		}
		inputFile.close();

		size_t maxLength;
		maxLength = vecLineLength[0].size();

		for( size_t i = 0; i < vecLineLength.size(); ++i){
			if(maxLength < vecLineLength[i].size()){
				maxLength = vecLineLength[i].size();
			}
		}
		return maxLength + 1;// 1 added to the length because '\n' is one character and was not included.

	}

	/////////////////////////////////////////////////////////////

	size_t uWordsCounter(string fileName){

		set<string> uniqueWords;

		ifstream inputFile(fileName.c_str(), ios::in);
		string str;
		while(!inputFile.eof()){
			inputFile >> str;
			uniqueWords.insert(str);
		}
		inputFile.close();

		return uniqueWords.size();

	}
