#include <cstdio>   // printf
#include <cstdlib>  // rand
#include <time.h>   // time
#include <getopt.h> // to parse long arguments.
#include <stdlib.h>
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iostream>
using std::cin;
using std::cout;
#include <algorithm>
using std::swap;
using std::min;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of shuf.  Supported options:\n\n"
"   -e,--echo              treat each argument as an input line.\n"
"   -i,--input-range=LO-HI treat each number in [LO..HI] as an input line.\n"
"   -n,--head-count=N      output at most N lines.\n"
"   --help                 show this message and exit.\n";

int main(int argc, char *argv[]) {
	// define long options
	static int echo=0, rlow=0, rhigh=0;
	static size_t count=-1;
	bool userange = false;
	static struct option long_opts[] = {
		{"echo",        no_argument,       0, 'e'},
		{"input-range", required_argument, 0, 'i'},
		{"head-count",  required_argument, 0, 'n'},
		{"help",        no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "ei:n:h", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'e':
				echo = 1;
				break;
			case 'i':
				if (sscanf(optarg,"%i-%i",&rlow,&rhigh) != 2) {
					fprintf(stderr, "Format for --input-range is N-M\n");
					rlow=0; rhigh=-1;
				} else {
					userange = true;
				}
				break;
			case 'n':
				count = atol(optarg);
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	/* NOTE: the system's shuf does not read stdin *and* use -i or -e.
	 * Even -i and -e are mutally exclusive... */

	/* TODO: write me... */
	//Done by Jonathan Luzincourt. My first attempt to hard code it with help from AManda Farhgli
	/*string lines;
	vector <string> v;
	while(getline(cin, lines))
	{
	v.push_back(lines);
	if(lines == "")
	break;
	}
	srand(time(NULL));
	int randm = rand()%v.size();
	vector<int> n;
	n.push_back(randm);
	
	int x = v.size() - 1; //process of checking duplication
	bool repeat = false;
	
	while(n.size() != x)
	{
	randm = rand()%x;
	for(size_t i = 0; i < n.size(); i++)
		{
		if(n.at(i) == randm) //checks randm
		repeat = true;
		}
		if(repeat == false)
		{
		n.push_back(randm); //puts randm into the vector
		repeat = false;
		}
	}
	for(size_t i = 0; i < n.size(); i++)
		{
		cout <<"\n" << v.at(n.at(i)); //checks if i is in the range of vector n within the range of vector v
		}
	*/
	//This is my attempt on implementing the arguments with flags
	string lines;
	vector <string> v;
	vector <string> temp;
	size_t randpos;
	while(getline(cin, lines)){
		v.push_back(lines);
		if(lines == ""){
			break;
		}
	srand(time(NULL));
	if(echo == 0 && useIflag == 0 && useNflag == 0){
		while(optind < argc){
			v.push_back(argv[optind++]);
			}
	}
	temp=v;
	else if(echo == 1 && useIflag == 0 && useNflag == 0){//for the -e flag. This prints the inputs randomly in separate lines
		for(unsigned i = 0; i < v.size(); i++){
			randpos = rand()%temp.size;
			//This ensures that there is no dulicates when randomizing
			cout << temp[randpos];
			temp.erase(temp.begin()+randpos);
			cout <<'\n';
			}
	}
	else if(echo == 0 && useIflag == 1 && useNflag == 0){//for the -i flag 
		
	}
	else if(echo == 0 && useIflag == 0 && useNflag == 1){//for the -n flag to limit the output to the users input
	
	}

	return 0;
}
/